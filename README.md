# SSHLab

Chooses an available host from the report published by ETSIT [here](https://labs.etsit.urjc.es/index.php/parte-de-guerra/) and opens a SHH session with it.
***
***By current version (V3.3.0), this host is chosen randomly over the less used hosts. The criteria is:***

1. ICMP is up and running.
2. SSH is up and running.
3. The host is not in "test mode".

Until here, this is only to determine the hosts available, but there's one more condition...

1. The host meets the requirements of the user. (CPU num and/or free memory available)

The script will try to choose a random host with less than N users connected once both "filters" are applied. If no available host is found, it will retry with N+1 users connected. By default the max users is set to 5, so starting from 0 users (No other users connected to that host) to a maximum of 5 users simultaneously.

The intention of this procedure is to distribute the users over the hosts the most homogeneous way possible.
***
***By current version (V3.3.0), Only the fingerprint of a host (Not any host, just the one the script is trying to connect to) which match with its fingerprint reported [here](https://labs.etsit.urjc.es/fingerprints-fue.txt) is added to the client (Spoiler: you!) See "Security concerns" section for further information.***
***

## Description

Fortunately for us, the students, the URJC has some very nice Linux labs. They just work fine most of the time, and believe me, this is not common. A public computer installation just working? Not even NASA lol.

However this has a downside, the excuse of "This works on my machine" is no longer available since the damn system is always up. So most of us ended up making the decision of working directly in the lab, but sometimes it is just physically impossible. Don't worry though, SSH came to the rescue.

Although the [team behind the ETSIT labs at URJC](https://labs.etsit.urjc.es/index.php/soporte/personal/) has done a very good work giving us plenty of alternatives to work with, I still miss one. If you want to open a SSH session **from your own machine**, you have to manually choose a host from the [report](https://labs.etsit.urjc.es/index.php/parte-de-guerra/) and blablabla...

So why the f@#$! did we invent computers then? Doing things manually, we, the humans? _Not in my backyard_. Let's write some code to fix this!

... Well, of course I am not ahead of the team mentioned before (In fact I am probably like a [lightyear](https://www.youtube.com/watch?v=rVqPBNBv9C8) behind) theirs [web application](https://labs.etsit.urjc.es/ssh/acceso-ssh-fue.php) does just the same thing what I am trying to accomplish here over the browser. But do you want to open a browser? I do not want to open a browser... Bad memories with browsers and _Bowser_.

![Bowser vs Browser](https://external-preview.redd.it/xyoXOLj6G-msad4KScAR6ynBqcipaUBKXTbghrYQ8Y4.png?auto=webp&s=51471648f0b7bec69589f76de542efb6720b8769)

But I'm not that sure if theirs system choose the most suitable host or distribute all the users over all the hosts possible, not even filtrate between them according to the user requirements... (What if you want more CPU's because you are about to build something there?)

## Usage

```sh
sshlab.sh -aA|-fF [--user foo] [--maxusers N] [--cpunum N] [--freemem N] [FLAGS...]
```
***
- [ ] --user
  * Username. Sets a different username to the SSH session. By default, this username **is the same as the user executing the script.**
- [ ] --maxusers
  * Max users. Sets the maximum of simultaneous users connected to a host to consider that host as a valid one. **Default is 5.**
- [ ] --cpunum
  * CPU number. Sets the minimum of CPU's number of a host to be considered as a valid one.
- [ ] --freemem
  * Free memory available. Sets the minimum of free memory of a host to be considered as a valid one.
***
- [ ] -a, -A
  * Alcorcón. Selects the lab of Alcorcón. **Is mandatory to choose between this flag or _-fF_**
- [ ] -c, -C
  * Classic. Enables _classic mode_ which avoids the automatic procedure of checking the fingerprints by searching for them in the ETSIT page. This means that `ssh` will warn you with the normal prompt if the script tries to connect to an unknown host. **Use this if the automatic procedure is not working properly.** (BTW, it is in fact working really well by the time this lines are being written)
- [ ] -f, -F
  * Fuenlabrada. Selects the lab of Fuenlabrada. **Is mandatory to choose between this flag or _-aA_**
- [ ] -h, -H
  * Help. Print the usage of the script.
- [ ] -q, -Q
  * Quiet. The script **does not output anything but the standard output of the ssh command** once the script finds a suitable host. ***THIS FLAG WILL OVERRIDE -vV***
- [ ] -t, -T
  * Text. The script **does not output anything but the first valid host** ***WITHOUT connecting to it.*** This can be useful if you only want a hostname to SCP, maybe... ***THIS FLAG ALSO ENABLES -qQ*** if '--user' is modified, the final message will include the username too.
- [ ] -v, -V
  * Verbose. Will print in its standard output info about the current process that is running.
***
You can either write the flags one by one or all together (Or a mix...), up to you!

## Visuals

***SCREENSHOTS ARE FROM V3.3.0***

***NOTICE THE WARNING PROMPT ON EVERY NEW HOST***

```sh
src/sshlab.sh -f
```
```sh
src/sshlab.sh -a
```
![Selecting a lab](/img/selecting-labs.png)

```sh
src/sshlab.sh -ft
```
```sh
src/sshlab.sh -at
```
```sh
src/sshlab.sh -ft --user foo
```
***NOTICE THE USERNAME BEFORE THE '@'***
![Text mode](/img/textmode.png)

```sh
src/sshlab.sh -fc
```
***NOTICE THE PROMPT***
![Classic mode](/img/classicmode.png)

```sh
src/sshlab.sh -f --user foo
```
***NOTICE THE USERNAME BEFORE THE '@'***
![Selecting a new username](/img/username.png)

```sh
src/sshlab.sh -fv --cpunum 6
```
***NOTICE HOW THE SCRIPT RETRIES WITH ANTOHER HOST AS `f-l3103-pc04.aulas.etsit.urjc.es` IDENTITY COULD NOT BE ASSURED***
![Adding requirements](/img/cpunum.png)

```sh
src/sshlab.sh -fq
```
![Quiet output](/img/quiet.png)

```sh
src/sshlab.sh -fv
```
![Verbose output](/img/verbose.png)

## Security concerns

Although SSH is a proved protocol, you may think that messing with it could compromise its reliability. And you are right.

But the [team behind the ETSIT labs at URJC](https://labs.etsit.urjc.es/index.php/soporte/personal/) have thought on this, so you can manually check the fingerprint of the host you're trying to connect to [here](https://labs.etsit.urjc.es/fingerprints-fue.txt).

The script takes advantage of this, so it checks for you the identity of the host by comparing the fingerprint retrieved by the host and the fingerprint shown on the [report](https://labs.etsit.urjc.es/fingerprints-fue.txt) for said host. **This is done on EVERY CONNECTION ATTEMPT TO EVERY HOST**.

### How it is done

Just after downloading [the hosts report](https://labs.etsit.urjc.es/index.php/parte-de-guerra/), [the fingerprint report](https://labs.etsit.urjc.es/fingerprints-fue.txt) is downloaded too. The script will just do its magic to find a suitable host (Or a bunch of them) according to your needs. Once the list of hosts is done, **BEFORE** attempting any connection the check procedure is called.

### What is the expected behaviour

They could be two scenarios when the fingerprint check procedure is called:

1. **The fingerprints match**. Hooray! We can assume that we trust enough the server so *we are safe* (Actually not. It's never safe enough, but...) The fingerprint is added to your machine at the *known_hosts* file, so no prompt will be shown on this attempt or in successive attempts for that host.
2. **The fingerprints don't match**. Oopsie doopsie... We could be in trouble. This does not directly mean that the server is hijacked, maybe the fingerprint report is just out of date and the machines are running without any issue. But *better be safe than sorry*, so the script will abort this attempt and retry with any other host.

And thats... not it! Actually, there is one more possible scenario:

3. The fingerprints match... **But you have already connected to that host before, and it does not match with the fingerprint stored in your machine**. *Every man for himself! We are under attack!* In this scenario is not the script but the `ssh` command itself who will warn you:

![ssh warn](https://i0.wp.com/virtuallywired.io/wp-content/uploads/2021/12/image.png?resize=720%2C164&ssl=1)

But as `ssh` pointed out, this is just a possibilty, not a certainty, so keep the knife down. You have the choice and **THE RESPONSIBILITY** of allow the connection or not, this script will no intervene on that. If you say `yes`, well, pretty self explanatory. If you say `no`, the script will try another hosts as nothing has happened.

### [What could pawssibly go wrong?](https://www.youtube.com/watch?v=1JTuZgo1Ooc)

If someone manage to fool you (or the script) and make you connect to its server, the most common thing is that... Just nothing happens or nothing works. **But you gave your credentials to that son of a b@#$! and he/she could connect to the actual server in your name!** Whatever you could do on that server... The attacker could do it too. The worst thing is that [he](https://twitter.com/AgutierrURJC) will be very upset if that bastard tries to do something nasty to the server and [he](https://twitter.com/AgutierrURJC) will believe that you are the attacker! (But we trust his knowledge so it will not take anytime to him to realize what really happened)

- [x] Friendly reminder: credentials-thingys are not a joke. If you think that they could be compromised change them ASAP... And please, check if it is included on [this list](https://en.wikipedia.org/wiki/List_of_the_most_common_passwords). `if found; then change it!; fi`

As we are connecting to the host by a DNS name, the only way the attacker could fool us is by changing the actual page of [the hosts report](https://labs.etsit.urjc.es/index.php/parte-de-guerra/) **AND** [the fingerprint report](https://labs.etsit.urjc.es/fingerprints-fue.txt) (That's why we are checking the fingerprints before connection, not only the attacker will have to make us point to his server but he also has to put its server's fingerprint on the report!) That means that the attacker will need to face the [crew](https://labs.etsit.urjc.es/index.php/soporte/personal/) mentioned before and theirs angry *picateclas* fingers. I'm not sure if its worth the effort for just download bad-written scripts like this one. ~~(Or maybe try to hack a teacher account, stealing the exams and reselling them for the trendy cryptocurrency...)~~

For the love of god make sure that you are actually running the same script that is on the [original repo](https://gitlab.etsit.urjc.es/dgarcu/sshlab) (Or at least, check it before running anything if you get it somewhere else) There is nothing to stop anyone to clone, modify and redistribute the repo. And only [God](https://twitter.com/obijuan_cube) knows its intentions...

BTW, there is some things that did not change:

1. Yes, you will have to enter your password on every connection. However, there is info on the _More useful info_ section at the end of this README if you want to [use a pair (public/private) of keys](https://labs.etsit.urjc.es/index.php/tutoriales/conectarnos-a-nuestra-cuenta-usando-autenticacion-basada-en-clave-publica-privada-desde-linux/).

## Support

Normally, you could get in touch with the OG programmer of the repo for any reason you may encounter while using it. But you know what, I'm so confident on you that I think that you could resolve any problem on your own. Clone it, modify it, make it yours, this is your new life! However I'm not that evil so, here is an alternative if you have nothing else to try:

- [ ] [I'm fully lost](https://www.youtube.com/watch?v=dQw4w9WgXcQ)

However as a rule of thumb, my grandfather used to say:
> Búscate la vida, gandul.

He made me learn [the hard way](https://www.youtube.com/watch?v=3ILscTrJrY4) though.

## Installation

This script should work in the [linux labs](https://labs.etsit.urjc.es/) (_Ubuntu 22.04 Jammy Jellyfish_) available on the URJC without installing anything. But this script was meant to run on your own machine.

But you may want to start a session by just a few keystrokes. How convenient will it be if you just can do...

```sh
sshlab
```

And boom! You are in the loggin interface of a valid host of your lab...

... Guess what.

Firstly, you have to store your script somewhere. Apparently, a nice place to this sort of things is `$HOME/bin`, maybe you want to do a deeper research on that. However, the place where you store your script is up to you!

Then you can edit your `$HOME/.bashrc` file to add an alias at the very end, which should like something like this:

```sh
alias sshlab='$HOME/bin/sshlab.sh -qf'
```

This is the result:

***VISUALS ARE FROM V3.3.0***

![Alias](/img/alias.png)

In my case, I choose this opts because I'm always connecting to Fuenlabrada with the same username as in my laptop or my home pc. You can modify the command as needed so it suits your requirements. Just think on how you normally will use the script and just type it as it is! Some examples could be...

```sh
alias sshlab='$HOME/bin/sshlab.sh -a'

```

```sh
alias sshlab='$HOME/bin/sshlab.sh -vf --user foo'
```

Furthermore, shell does a literal substitution of the alias with whatever you write even before execute nothing. This means that you have not to write another alias (Or execute the script from its location) if you want to slightly change some parameters, like the user, for example:

```sh
sshlab --user foo
```

Before executing anything, the shell turns that in...

```sh
$HOME/bin/sshlab.sh -qf --user foo
```

So, the final result looks like this:

***NOTICE THE USERNAME BEFORE THE '@'***
![Alias with parameters 2](/img/alias2.png)

Maybe you want to reach a host with 6 CPU's minimum...

```sh
sshlab --cpunum 6
```

Remember, this will be _translated_ by the shell to:

```sh
$HOME/bin/sshlab.sh -qf --cpunum 6
```
![Alias with parameters 3](/img/alias3.png)

If we check the host `f-l3202-pc08.aulas.etsit.urjc.es` in the [report](https://labs.etsit.urjc.es/parte/index.php), we will see that actually, this hosts has 6 CPU's (4th column from right), so it meets our requirements!

![f-l3202-pc08.aulas.etsit.urjc.es data](/img/reportcpunum.png)

And what looks like to have an alias and [a pair (public/private) of keys](https://labs.etsit.urjc.es/index.php/tutoriales/conectarnos-a-nuestra-cuenta-usando-autenticacion-basada-en-clave-publica-privada-desde-linux/)... Check it out by yourself!

```sh
sshlab
```
![Alias and pair of keys](/img/alias+keypair.png)

I'm in! Very handy, *innit*?

## Exit errors

Although the script will always exit with an error message, here you can find a more detailed explanation of every error contemplated. The script will exit with that exact number too.

1. Usage procedure was called.
2. The argument passed to the script does not match anything meaningful.
3. Unknown option passed to the script.
4. The argument passed to `--user` does not look like a user name. It should pass the following regex:
- `^[a-z_]([a-z0-9_-]{0,31}|[a-z0-9_-]{0,30}\$)$`
5. The argument passed to `--maxusers` is not a natural number.
6. The argument passed to `--cpunum` is not a natural number.
7. The argument passed to `--freemem` is not a natural or rational positive number.
8. The flag passed is not known.
9. Multiple labs were selected by multiple flags. Only one is allowed.
10. No lab was selected by any flag.
11. Could not download [the hosts report](https://labs.etsit.urjc.es/index.php/parte-de-guerra/) from the source page due to a problem. This could be connection issues, either by the server or by the client.
12. Could not download [the fingerprint report](https://labs.etsit.urjc.es/fingerprints-fue.txt) from the source page due to a problem. This could be connection issues, either by the server or by the client.
13. There is no available hosts on the lab selected (ICMP should be up, SSH should be up and 'test mode' should be disabled to consider a host as available).
14. The argument passed to `--cpunum` is greater than any of the available hosts' CPU number.
15. The argument passed to `--freemem` is greater than any of the available hosts' free memory.
16. Could not connect to any of the host available of the lab selected (This could be triggered if there's no host available with less than N users connected, either set by `--maxusers` or just **five by default**)

## Authors and acknowledgment

Although I am the only author for this humble piece of code, none a single letter could be written without their help:

- [ ] [He taught me git and shell scripting](https://twitter.com/paurea).
- [ ] [He is the reason why I can publish this repo for you](https://twitter.com/AgutierrURJC).
- [ ] [They are the team behind the ETSIT labs, its development and maintenance](https://labs.etsit.urjc.es/index.php/soporte/personal/).

## License

Currently considering [MIT](https://choosealicense.com/licenses/mit/) license.
But in reality I do not know anything about licenses and I am not sure how can I apply for one or how can this license affect this project.
So feel free to do whatever the heck you want with this _Mumbo Jumbo_...

![ours](https://c.tenor.com/PA7_rZlLgbkAAAAC/meme-our.gif)

## Project status

Working according to the Roadmap.

## Roadmap

- [x] Add opt to choose between Fuenlabrada or Alcorcón labs.
- [x] Add opt to choose the username for the loggin.
- [x] Retrieve the host-name of all the machines which are available.
- [x] Add a short tutorial to make alias in order to simplify the use of the script.
- [x] Try to develop a simple algorithim which can choose the most suitable machine if possible (No more than N users, low CPU load...)
- [x] Add more opts so the user can choose a host with some minimum requirements (Number of CPUs, free memory available...)
- [x] ~~Add a _subscript_ to download and store all the fingerprints of the hosts available in the user machine~~ Check the fingerprint retrieved by the host with the fingerprint showed [here](https://labs.etsit.urjc.es/fingerprints-fue.txt) before connection, so no fingerprint prompt will be shown at any host, but keeping the security that a fingerprint provides.
- [x] Add opt to avoid the fingerprint check procedure and stick with the classic method, confirming the '_not known host_' prompt manually.
- [x] Add opt to only show through StdOut the hostname of the first valid host found. Useful if you want to use this host for other services like SCP.

### More useful info

Go check the labs [tutorials](https://labs.etsit.urjc.es/index.php/tutoriales/), you might find something useful. (Tired of typing your password every time you want to start an SSH session, maybe...?) as well as the [etsit repo](https://labs.etsit.urjc.es/repo/).
