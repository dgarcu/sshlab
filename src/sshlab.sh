#!/bin/sh

export LANG='en_US.UTF-8'
# Ensure that the enviroment where the script is being executed looks like the enviroment where it was developed and tested.

#==================================#

version='V3.3.0'

#==================================#

verbose='false'
quiet='false'
textOutput='false'
classicMode='false'

#==================================#

reportUrl=''
fingerprintsUrl=''
campus=''
user=$(whoami)
hostSuffix='.aulas.etsit.urjc.es'
maxUsersAttempt='5'
sshDisableCheckArg='-o StrictHostKeyChecking=no'

#==================================#

usage(){
	echo "usage: $0 -aA|-fF [--user user] [--maxusers N] [--cpunum N] [--freemem N] [FLAGS...]"
	echo "
	--user\tSets a username for the SSH session.
	--maxusers\tSets a maximum of users connected to a host to filter them. (Script will iterate from zero to max) Default: 5.
	--cpunum\tSets a minimum of N CPU's required on a host to be considered as valid.
	--freemem\tSets N as the minimum of free memory available in a host to be considered as valid.
	-aA\tSelects Alcorcón's labs.
	-cC\tEnables 'Classic Mode'. This avoids the automatic procedure of checking the fingerprints by searching for them on the report at ETSIT page and sticks with the normal prompt on every new host.
	-fF\tSelects Fuenlabrada's labs.
	-hH\tShows this usage message.
	-qQ\tDo not show the script version or error messages from SSH.
	-tT\tShow only the first valid host thought StdOut but no connect to it. This enables also -qQ.
	\tWarning: if '--user' is modified, the final message will include the username too:
	\t\t- foo@an.awesome.host.url.com
	-vV\tShow info about the current process that is running."
	exit 1
}

err(){
	echo "Error: $1"
	exit "$2"
}

#===================================#

isQuiet(){
	test "$quiet" = 'true'
}

isVerbose(){
	test !isQuiet && test "$verbose" = 'true'
}

#===================================#

checkCampus(){
	if [ -n "$reportUrl" ] || [ -n "$campus" ]; then
		err "$campus's lab already selected. Please select only one lab by adding -aA or -fF flag." 9
	fi
}

#==================================#

processFlag(){
	for flag in $(echo "$1" | grep -o .); do # Splits the argument string in each character
		case "$flag" in
			[aA])
				checkCampus
				reportUrl='https://labs.etsit.urjc.es/parte/index.php?campus=alcorcon'
				fingerprintsUrl='https://labs.etsit.urjc.es/fingerprints-alc.txt'
				campus='Alcorcón'
				;;
			[cC])
				classicMode='true'
				sshDisableCheckArg=''
				;;
			[fF])
				checkCampus
				reportUrl='https://labs.etsit.urjc.es/parte/index.php'
				fingerprintsUrl='https://labs.etsit.urjc.es/fingerprints-fue.txt'
				campus='Fuenlabrada'
				;;
			[hH])
				usage
				;;
			[qQ])
				quiet='true'
				;;
			[tT])
				textOutput='true'
				quiet='true'
				;;
			[vV])
				verbose='true'
				;;
			*)
				err "unknow flag \"$flag\"" 8
				;;
		esac
	done
}

processOption(){
	option=$(echo "$1" | tr '[:upper:]' '[:lower:]')
	case "$option" in
		user)
			user="$2" # The next argument should be the username.
			# Check if it is a valid username.
			if ! echo "$user" | egrep -q '^[a-z_]([a-z0-9_-]{0,31}|[a-z0-9_-]{0,30}\$)$'; then
				err "\"$user\" seems like is not a valid user name." 4
			fi
			;;
		maxusers)
			maxUsersAttempt="$2" # The next argument should be the max users connected to a host.
			# Check if it is a natural number.
			if ! echo "$maxUsersAttempt" | egrep -q '^[[:digit:]]+$'; then
				err "max users should be a natural number (\"$maxUsersAttempt\" passed)" 5
			fi
			;;
		cpunum)
			cpuNum="$2" # The next argument should be the number of CPUs the user is asking for.
			# Check if it is a natural number.
			if ! echo "$cpuNum" | egrep -q '^[[:digit:]]+$'; then
				err "CPU num should be a natural number (\"$cpuNum\" passed)" 6
			fi
			;;
		freemem)
			freeMem="$2" # The next argument should be the free memory available the user is asking for.
			# Check if it is a natural or positive rational number.
			if ! echo "$freeMem" | egrep -q '^[[:digit:]]+$|^[[:digit:]]+\.[[:digit:]]+$'; then
				err "Free memory should be a natural or a positive rational number (\"$freeMem\" passed)" 7
			fi
			;;
		*)
			err "unknow option \"$1\"" 3
			;;
	esac
}

#==================================#

while [ $# -gt 0 ]; do # While there's arguments to process...
	case "$1" in
		--*[a-zA-Z]) # In case that's a "--" followed by any letter
			processOption $(echo "$1" | cut -c 3-) "$2" # rip off the "--" before pass and the next argument
			shift 2 # Two arguments were processed so it is necessary skip by two.
			continue # This avoid the shift located below as we called it right before this line.
			;;
		-*[a-zA-Z]) # In case that's a "-" followed by any letter
			processFlag $(echo "$1" | cut -c 2-) # rip off the "-" before pass.
			;;
		*)
			err "unknow argument \"$1\"" 2
			;;
	esac
	shift
done

#==================================#

repeatChar(){
	for i in $(seq 1 "$2"); do echo -n "$1"; done
	echo
}

greeting(){
	scriptName=$(echo "$0" | sed -E 's-(^.+/)(.+)(\.sh$)-\2-')
	vmsg="***** $scriptName $version *****"
	echo
	for i in $(seq 1 5); do
		if [ $i -ne 3 ]; then
			repeatChar '*' ${#vmsg}
		else
			echo "$vmsg"
		fi
	done
	echo
}

#===================================#

filterHosts(){
	# As hosts are filtered one by one, they are concatenated to this variable.
	# So it is necessary to 'reset' the variable since the filtering process will concatenate the new hosts in the next iteration.
	filteredHosts=''
	for labHost in $availableHosts; do
		hostData=$(echo "$reportClean" | egrep -A 11 "$labHost")
		filteredHosts=${filteredHosts}$(echo $hostData | awk -v cpunum="$cpuNum" -v freemem="$freeMem" -v maxUsers="$1" '{
			filter = $6 <=maxUsers && $7 <=maxUsers
			if (cpunum != "")
				filter = filter && $9 >= cpunum
			if (freemem != "")
				filter = filter && $12 >= freemem
			if (filter)
				print $1 " "
		}')
	done
	# To prevents errors, if no hosts are included to the variable (A.K.A only has blank spaces due to unexpected behaviour with the AWK script), then ensure that the variable is returned empty.
	if echo -n "$filteredHosts" | egrep '^[[:blank:]]*$'; then filteredHosts=''; fi;
}

#===================================#

cleanReport(){
	# Get rid of everything that is not part of the table data.
	reportClean=$(echo "$1" | egrep 'table-data')
	# Get rid of everything that is before where the actual data starts.
	# This means all blanks spaces and the first angle brackets found in the line.
	reportClean=$(echo "$reportClean" | sed -e 's/^[[:blank:]]*<[^<>]*>//')
	# Get rid of everything that is after where the actual data finishes.
	# This means all that is after the last angle brackets, included.
	reportClean=$(echo "$reportClean" | sed -e 's/<[^<>]*>$//')
	# There is an icon in the html called 'thumbs-up.png' or 'thumbs-down.png', which is used to display which attributes from the host are ok or not.
	# These whole lines are replaced by a 'YES' or 'NO' to easy manipulation as they are originally complex.
	reportClean=$(echo "$reportClean" | sed -e 's/.*thumbs-up.*/YES/;s/.*thumbs-down.*/NO/')

	# This structure is equal for each host (in AWK each line will be referenced as $N):
	# 1.- ($1 in AWK) Host name
	# 2.- ($2 in AWK) ICMP status
	# 3.- ($3 in AWK) SSH status
	# 4.- ($4 in AWK) 'Test mode' status
	# 5.- ($5 in AWK) Number of process.
	# 6.- ($6 in AWK) Users (VNC)
	# 7.- ($7 in AWK) Users (SSH)
	# 8.- ($8 in AWK) Load average (1M)
	# 9.- ($9 in AWK) CPU number
	# 10.- ($10 in AWK) Usage of CPU (%)
	# 11.- ($11 in AWK) total memory (GB)
	# 12.- ($12 in AWK) free memory available (GB)
}

getHosts(){
	# Phisical hosts are named in this format: X-lYYYY-pcYY, where X is a letter between 'a' or 'f' and Y is a digit.
	# In the other hand, virtual hosts follow this format: X-l-vmYY, where X is a letter between 'a' or 'f' and Y is a digit.
	allHosts=$(echo "$1" | egrep -e 'f-l[[:digit:]]+-pc[[:digit:]]+' -e 'a-gs[[:digit:]]+-pc[[:digit:]]+' -e '[af]-l-vm[[:digit:]]+')
}

#===================================#

isFingerprintValid(){
	# Get the fingerprint key from the host in the same format as they are showed on the fingerprint report.
	hostKey=$(ssh-keyscan -Ht ecdsa "$labHost$hostSuffix" 2> /dev/null | ssh-keygen -l -E sha256 -f - | sed -E 's/(.+)(SHA256:.{43})(.+)/\2/')
	# Get the fingerprint key from the fingerprint report of said host
	reportKey=$(echo "$fingerprintReport" | egrep "$labHost$hostSuffix" -A 4 | egrep "(ECDSA)" | sed -E 's/(.+)(SHA256:.{43})(.+)/\2/')

	# They could be hosts not shown on the fingerprint report
	if [ -z "$reportKey" ]; then reportKey='NOT FOUND'; fi

	if [ "$hostKey" = "$reportKey" ]; then
		return 0
	else
		if ! isQuiet; then
			echo "Warning: could not check the identity of $labHost$hostSuffix:\n\tRetrieved fingerprint by $labHost$hostSuffix (ECDSA):\t$hostKey\n\tRetrieved fingerprint by $fingerprintsUrl for $labHost$hostSuffix (ECDSA):\t$reportKey\nTrying with another host..."
		fi
		return 1
	fi
}

#===================================#

if [ -z "$reportUrl" ] || [ -z "$campus" ] || [ -z "$fingerprintsUrl" ]; then
	err "no lab selected. Please select Alcorcón or Fuenlabrada labs by adding -aA or -fF flag." 10
fi

if ! isQuiet; then
	greeting
fi

if isVerbose; then
	echo "Downloading report from $campus's labs, please wait... You can check it out at $reportUrl"
fi

htmlReport=$(wget -qO- "$reportUrl")

if [ "$classicMode" = 'false' ]; then
	fingerprintReport=$(wget -qO- "$fingerprintsUrl")
fi

if [ -z "$htmlReport" ]; then
	err "could not download the report of $campus's labs from $reportUrl." 11
fi

if [ -z "$fingerprintReport" ] && [ "$classicMode" = 'false' ]; then
	err "could not download the report of fingerprints from $fingerprintsUrl." 12
fi

if isVerbose; then
	echo "Report downloaded successfully. Searching for available hosts, please wait..."
fi

cleanReport "$htmlReport"

getHosts "$reportClean"

for labHost in $allHosts; do
	# Grab the host name and its next 11 lines, as they are data from said host.
	hostData=$(echo "$reportClean" | egrep -A 11 "$labHost")
	# $12 is the free memeory available.
	maxFreeMem=$(echo $hostData | awk -v max="$maxFreeMem" '{
		if ($12 > max)
			print $12
		else
			print max
	}')
	# $9 is the number of cpus.
	maxCpuNum=$(echo $hostData | awk -v max="$maxCpuNum" '{
		if ($9 > max)
			print $9
		else
			print max
	}')
	# $2 is ICMP, $3 is SSH and $4 is "test mode"
	availableHosts=${availableHosts}$(echo $hostData | awk '{
		if ($2 =="YES" && $3 =="YES" && $4 =="NO")
			print $1 " "
	}')
done

if [ -z "$availableHosts" ]; then
	err "No available hosts found in $campus's labs. Try reach https://labs.etsit.urjc.es/index.php/soporte/ for further assistance." 13
fi

if [ -n "$cpuNum" ] && [ "$cpuNum" -gt "$maxCpuNum" ]; then
	err "there is no host with $cpuNum CPU's (max: $maxCpuNum)" 14
fi

if [ -n "$freeMem" ] && [ 1 -eq "$(echo "${freeMem} > ${maxFreeMem}" | bc)" ]; then
	err "there is no host with $freeMem GB of free memory (max: $maxFreeMem GB)" 15
fi

if isVerbose; then
	numAvailableHosts=$(echo -n "$availableHosts" | tr ' ' '\n' | wc -l)
	echo "Searching completed. $numAvailableHosts host(s) found."
	userFilter="Filtering hosts according to your needs:\n\tNumber of users connected: 0 or less."
	# Add more info to the verbose message in case it is necessary
	if [ -n "$cpuNum" ]; then userFilter="$userFilter\n\tNumber of CPU's (minimum): $cpuNum"; fi;
	if [ -n "$freeMem" ]; then userFilter="$userFilter\n\tFree memory available (minimum): $freeMem gb"; fi;
	# Print the whole message
	echo "$userFilter"
fi

for i in $(seq 0 "$maxUsersAttempt"); do
	filterHosts "$i"

	if [ -z "$filteredHosts" ]; then
		if isVerbose; then
			userFilter="No available hosts found in $campus's labs with $i or less users connected. Trying with:\n\tNumber of users connected: $(($i+1)) or less."
			# Add more info to the verbose message in case it is necessary
			if [ -n "$cpuNum" ]; then userFilter="$userFilter\n\tNumber of CPU's (minimum): $cpuNum"; fi;
			if [ -n "$freeMem" ]; then userFilter="$userFilter\n\tFree memory available (minimum): $freeMem gb"; fi;
			# Print the whole message
			echo "$userFilter"
		fi
		continue
	fi

	if isVerbose; then
		numFilteredHosts=$(echo -n "$filteredHosts" | tr ' ' '\n' | wc -l)
		echo "Filtering completed. $numFilteredHosts host(s) found."
	fi

	# Randomize the order of the hosts.
	filteredHosts=$(echo -n "$filteredHosts" | tr ' ' '\n' | sort -R | tr '\n' ' ')

	# Change the file where the output of the ssh command will be redirected according to a variable's state.
	sshErrOutput="/proc/$$/fd/2" # This is the default redirect.
	if isQuiet; then
		sshErrOutput='/dev/null' # If the script is executed as quiet...
	fi

	for labHost in $filteredHosts; do
		if isVerbose; then
			echo "Trying to connect to $labHost$hostSuffix as $user:"
		fi

		if [ "$classicMode" = 'false' ]; then
			if ! isFingerprintValid; then continue; fi # If the fingerprint retrieved by the host does not match the fingerprint on the report, try with next host.
		fi

		if [ "$textOutput" = 'true' ]; then
			if [ "$user" != $(whoami) ]; then
				echo -n "$user@"
				# Merge the final message with the username if it is not the same as the user executing the script.
			fi
			echo "$labHost$hostSuffix"
			exit 0
		fi

		ssh $sshDisableCheckArg -l "$user" "$labHost$hostSuffix" 2> "$sshErrOutput" && exit 0
		# StrictHostKeyChecking is not enabled by default since a procedure to trust the integrity of the host was already executed at this point.
		# In fact, what this option does is to add the host to the "known_hosts" file automatically if not found there.
	done
done

err "could not connect to any of the available hosts." 16
# Reach this point means that the script iterated over all the hosts and could not connect to any of them.
